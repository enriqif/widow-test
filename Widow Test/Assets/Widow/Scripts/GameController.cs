﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class GameController : MonoBehaviour
{
    private const string URL = "https://api.pubg.com/tournaments";
    private const string API_KEY = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJkNGU2OWZiMC0wYTljLTAxMzktNGY5NS0zZDc5OTZkODI4NjgiLCJpc3MiOiJnYW1lbG9ja2VyIiwiaWF0IjoxNjA1NTc0MjM3LCJwdWIiOiJibHVlaG9sZSIsInRpdGxlIjoicHViZyIsImFwcCI6IndpZG93In0.3YWBzYnWy5k0SBvpAMYw0H1_zwa69ftckmvJ4k5CM18";

    [SerializeField]
    private Tournaments info;

    [SerializeField]
    private GameObject slot_prefab;
    
    [SerializeField]
    private GameObject content;
        
    void Start()
    {
        StartCoroutine(GetTournament());
    }

    public IEnumerator GetTournament()
    {
        UnityWebRequest req = UnityWebRequest.Get(URL);
        req.SetRequestHeader("Authorization", API_KEY);
        req.SetRequestHeader("accept", "application/vnd.api+json");

        yield return req.SendWebRequest();

        if (req.isNetworkError || req.isHttpError)
        {
            Debug.LogError(req.error);
        }
        if(req.isDone)
        {
            Debug.Log("DATA: " + req.downloadHandler.text);
            string data = req.downloadHandler.text;
            JsonUtility.FromJsonOverwrite(data, info);

            for (int i = 0; i < info.data.Count; i++)
            {
                CreateObjectInUI(info.data[i].type, info.data[i].id, info.data[i].attributes.createdAt);
            }
        }
    }

    private void CreateObjectInUI(string type, string id, string createdAt)
    {
        GameObject slot = Instantiate(slot_prefab);

        Text txt_id = slot.transform.GetChild(0).GetComponent<Text>();
        txt_id.text = id;

        Text txt_createdAt = slot.transform.GetChild(1).GetComponent<Text>();
        txt_createdAt.text = createdAt;

        Debug.Log("CREATED : " + createdAt);

        slot.transform.parent = content.transform;
    }
}
