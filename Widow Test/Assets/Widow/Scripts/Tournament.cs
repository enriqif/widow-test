﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Attribute
{
    public string createdAt;
}

[System.Serializable]
public class Tournament
{
    public string type;
    public string id;
    public Attribute attributes;
}

[System.Serializable]
public class Link
{
    public string self;
}

[System.Serializable]
public class Meta
{
    
}

[System.Serializable]
public class Tournaments {

    [SerializeField]
    public List<Tournament> data;

    [SerializeField]
    public Link links;

    [SerializeField]
    public Meta meta;
}
